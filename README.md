# dev-hbase-1.2.4
Manages docker image for local development.

### Commands
- `make start`
- `make stop`
- `make check-running`
